# Dynamic Pipelines

This relies on Jinja2 Formatting.

* You **must** have a `dynamic.yml.j2` file available to be used as a template.
* You **must** store the create job ID in a `.env` file.

The use case is that I have a monorepo and want to automatically run a CI Pipeline
for any changed code without requiring the sub-directories (aka Applications) to have
to know anything about GitLab CI.  They code their app, and when things change, pipelines
are kicked off.


This job additionally will kick off a child pipeline and download all artifacts from those pipelines
and store them as artifacts in the parent pipeline so that they're available in the parent pipeline 
and able to be used as reports.


## Transfering Artifacts From Parent to Child and Vice Versa

Currently there is no solution for transferring artifacts between parent child pipelines (see issues below).

* [Artifacts are not downloaded on Child Pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/213457)
* [Parent of Child Pipelines missing Child Artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/215725)

The `downloader.py` script is used to make artifacts accessible both to the child (from the parent) and to the parent
(from the child).

**Requirements**:

* Have a script in the parent that saves a `.env` file containing a job ID from the first stage of the parent pipeline as `CREATE_JOB_ID`.
* Pass the Parent Pipeline ID as a variable to the child pipelines as `PARENT_PIPELINE_ID`
* Save a Private Access Token as `PRIVATE_TOKEN`
* Expect all parent/child artifacts to be stored in a `jobs` directory after the `download.py` runs.
